// SPDX-License-Identifier: CC-by-nc-nd-4.0-or-later
pragma solidity 0.8.13;

import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable {
    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
    }
    
    struct Proposal {
        string description;
        uint voteCount;
    }
    
    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }
    
    uint public winningProposalId;
    WorkflowStatus public status;
    
    mapping(address => Voter) public voters;
    Proposal[] public proposals;
    
    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted(address voter, uint proposalId);
    
    constructor(address[] memory _whiteList) {
        status = WorkflowStatus.RegisteringVoters;
        for(uint i=0; i<_whiteList.length; i++){
            registerVoter(_whiteList[i]);
        }
    }
    
    modifier onlyDuringStatus(WorkflowStatus _status) {
        require(status == _status, "Invalid workflow status");
        _;
    }
    
    function registerVoter(address _voterAddress) public onlyOwner onlyDuringStatus(WorkflowStatus.RegisteringVoters) {
        require(!voters[_voterAddress].isRegistered, "Voter already registered");
        voters[_voterAddress].isRegistered = true;
        emit VoterRegistered(_voterAddress);
    }
    
    function startProposalsRegistration() public onlyOwner onlyDuringStatus(WorkflowStatus.RegisteringVoters) {
        status = WorkflowStatus.ProposalsRegistrationStarted;
        emit WorkflowStatusChange(WorkflowStatus.RegisteringVoters, status);
    }
    
    function endProposalsRegistration() public onlyOwner onlyDuringStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        status = WorkflowStatus.ProposalsRegistrationEnded;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationStarted, status);
    }
    
    function startVotingSession() public onlyOwner onlyDuringStatus(WorkflowStatus.ProposalsRegistrationEnded) {
        status = WorkflowStatus.VotingSessionStarted;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationEnded, status);
    }
    
    function endVotingSession() public onlyOwner onlyDuringStatus(WorkflowStatus.VotingSessionStarted) {
        status = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionStarted, status);
    }
    
    function tallyVotes() public onlyOwner onlyDuringStatus(WorkflowStatus.VotingSessionEnded) {
        status = WorkflowStatus.VotesTallied;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionEnded, status);
        
        uint maxVoteCount = 0;
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > maxVoteCount) {
                maxVoteCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }
    }
    
    function submitProposal(string memory _description) public onlyDuringStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        require(voters[msg.sender].isRegistered, "Voter not registered");
        proposals.push(Proposal(_description, 0));
        emit ProposalRegistered(proposals.length - 1);
    }
    
    function vote(uint _proposalId) public onlyDuringStatus(WorkflowStatus.VotingSessionStarted) {
        require(voters[msg.sender].isRegistered, "Voter not registered");
        require(!voters[msg.sender].hasVoted, "Voter already voted");
        require(_proposalId < proposals.length, "Invalid proposal ID");
        
        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = _proposalId;
        proposals[_proposalId].voteCount++;
        
        emit Voted(msg.sender, _proposalId);
    }
    
    function getWinner() public view returns (string memory) {
        require(status == WorkflowStatus.VotesTallied, "Votes not tallied yet");
        return proposals[winningProposalId].description;
    }
}