## Table des Matiéres

[TOC]

## Description

Ce dépot contient le projet individuel de [R5.A.13 - Eco durable et num]

## Usage

- ### Récuperation

```bash
git clone https://gitlab.univ-lille.fr/pierre.dereu.etu/smart-contract
```

- ### Mise en place

    aprés récupération :
    ```bash
    cd solidity-smart-contract
    ```

    installer les dépendances et démarrer le serveur de test
    ```bash
    npm i --no-optional
    npx ganache-cli --account_keys_path keys.json
    ```
    dans un nouveau terminal dans le méme dossier
    ```bash
    npm run full
    ```

*   *   *

À présent les contrats sont déployés sur le serveur de test
a l'adresse : [127.0.0.1:8545](http://127.0.0.1:8545)

## License
L'ensemble des productions sur ce dépot sont couvertes par la licence Creative Commons by-nc-nd 4.0.  
[<img src="https://licensebuttons.net/l/by-nc-nd/3.0/88x31.png">](https://creativecommons.org/licenses/by-nc-nd/4.0/ "License")
